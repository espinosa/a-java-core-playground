# Solutions for mixed type equals()

Equals have OOP inheritance have an uneasy relationship. Joshua Bloch famously stated that "there is no way to
extend an instantiable class and add a value component while preserving the equals contract"
(page 38, Effective Java, item 8). He suggest as only solution to always use Composition over
Inheritance. But what if you have to stick Inheritance? 

I am going to introduce you 5 (!) different solution for convenient comparison. 
To demonstrate the principles notorious classes Color and ColorPoint are used.
You find them in Joshua Bloch book, in official Java Language Specification or in Martin Odersky's blog
so you should be familiar with them. All variants are accompanied with relevant unit tests.
I wanted to avoid embarrassment of some other bloggers (Tal Cohen ;) of presenting 
broken code.

It is difficult to reconcile mixed type equality comparisons where subclass adds extra key fields.   
Problem is Point does not know about extra field in ColorPoint, so from its
perspective they are equal, but from ColorPoint they are not - broken symmetry.

To demonstrate the problem I have prepared this two classes

   * [A1EqualsAndInheritanceUntreatedCodeBreaksSymmetry](A1EqualsAndInheritanceUntreatedCodeBreaksSymmetry.java) - 
     Presents implementation of equals()  based on instaceof class comparison and test it 
     on mixed type comparisons of Point and ColorPoint.
     This breaks symmetry when extra field  
     
   * [A2EqualsAndInheritanceUntreatedCodeBreaksLSP](A2EqualsAndInheritanceUntreatedCodeBreaksLSP.java) - 
     Presents implementation of equals() provided by generators from famous IDEs like Eclipse or IntelliJ Idea
     based on strict class comparison - this.getClass().equals(other.getClass()).
     While it solves problem with broken symmetry and adheres fully to equals() contract,
     it blocks any mixed type comparisons, even between compatible classes like Point and PointAdapter. 
     This breaks Liskov Substitution Principle! 
   
  
## Restricted mixed type equals() solutions
 
Solutions for Mixed Type equals() where Point genuinely cannot be equal to ColorPoint (extra field), 
but can be equal to PointAdapter (no extra fields)   

   * **Tal Cohen's blindlyEquals** solution - included mainly  because is often cited. 
     It is the least effective solution but it is most intuitive and therefore good to start with.
     It is named after a required helper method – blindlyEquals. The equality is established when 
     both parties agree that they are blindly equal to each other. The drawback is that 
     all fields has to be checked twice, for every direction.  
     See [A3EqualsAndInheritanceTalCohenSolution](A3EqualsAndInheritanceTalCohenSolution.java)  
    
   * **Rene Smith's getEquivalenceClass** solution - as a reaction to Tal Cohen's
     Included mainly for completeness sake. It is in fact just a variant of more widely known Martin Odersky's 
     canEqual solution. Helper method getEquivalenceClass() specify what is the minimal required class type
     for equality comparison. That way Color is not compatible with ColorPoint, but ColorPointDelegator can be
     compatible and can equal - LSP i saved.  
     See [A4EqualsAndInheritanceReneSmitSolution](A4EqualsAndInheritanceReneSmitSolution.java)  
    
   * **Martin Odersky's canEqual** solution - where canEquals() is a helper method. This solution is used 
     in Scala. Variant of previous example. This time helper method canEquals() does the whole class type 
     comparison inside, it specifies minimal required class type internally, returning result as boolean.   
     See [A5EqualsAndInheritanceMartinOderskySolution](A5EqualsAndInheritanceMartinOderskySolution.java)  
   
   * my own **Simple Reversing** solution - my own solution, inspired by Angelika Langer solution.
     I strongly suggest this solution as it is the simplest and does not require any helper method.  
     See [A6EqualsAndInheritanceSimpleReversingSolution](A6EqualsAndInheritanceSimpleReversingSolution.java)  
     
     Simple Reversing solution goes in two variants. Second variant uses repeated reversion blocking flag.  
     Then we can afford revert direction even for same classes, dropping need for that extra check.
     It makes some thins simpler, and perhaps is a bit more performant, but it requires 
     helper method - flagged version of equals().  
     See [A6EqualsAndInheritanceSimpleReversingSolutionAlt](A6EqualsAndInheritanceSimpleReversingSolutionAlt.java).
   
   
## Unrestricted mixed type equals() solution

Solutions for mixed type equals() where Point genuinely can be equal to ColorPoint. In this case Point 
is considered a point with unspecified color, that is default color, let's say it is colorless by default, 
and therefore it can be compared to ColorPoint.

This approach assume every key field has a specified default value. If only one field cannot be assigned
a default value then types are deemed incompatible for mixed type comparison. There can be more than one 
default value per field but let's not complicate it for now.    

### Angelika Langer's Traverse Hierarchy solution

It is the only solution of all listed here, capable of comparing classes across different
inheritance branches! You can compare Point3D with ColorPoint3D with ColorPoint or with root Point.
This solution is fully and unarguably and 100% Liskov Substitution Principle compliant.
It is also the most difficult to understand.
 
I present Angelika Langer's implementation in two two gradual steps:

   * First [A7EqualsAndInheritanceAngelikaLangerSolution](A7EqualsAndInheritanceAngelikaLangerSolution.java) contains 
     full implementation with same set of test as in previous examples, for easy comparison. Attached unit 
     tests show that now ColorPoint can be equals to Point if the point has default color (that it is colorless)
     and symmetry is not broken.       
     
   * To demonstrate full power of Angelika Langer's hierarchy traversing solution I have to add extra 
     sample classes and couple of new tests. Check out 
     [A7EqualsAndInheritanceAngelikaLangerSolutionFull](A7EqualsAndInheritanceAngelikaLangerSolutionFull.java) 
     The new classes are *Point3D* and *ColorPoint3D*; Point3D extends Point directly and  ColorPoint3D, as the 
     name suggests, extends ColorPoint.  *Two separate hierarchy branches* formed here. Despite this extra 
     complication we are able to match them equal when common fields are equal and specific fields having 
     default values. That is ColorPoint3D(2, 3, 0, Color.TRANSPARENT) is equal ColorPoint(2, 3, Color.TRANSPARENT) 
     or Point(2,3) or Point3D(2,3,0).  	


------------------------------------------------------------------------------------------------------
Notes:
  
   * Unit testing is by no means extensive, its main purpose is to demonstrate the presented principle
     and provide ready to debug code for relevant cases.

   * For demonstration purposes I include whole code for discussed case into one file.
     Tests are mixed with tested implementation. This is not recommended for regular unit test
     an real projects. It is not my normal way of unit testing!
       