package my.code.a003.equality;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Equals and inheritance - uneasy relationship. According to Joshua Bloch "there is no way to
 * extend an instantiable class and add a value component while preserving the equals contract"
 * (page 38, Effective Java, item 8). Joshua Bloch suggest as only way to use Composition over
 * Inheritance. But what if you have to stick Inheritance?
 * <p>
 * This is my own solution for restricted mixed type equality comparisons. I was inspired by
 * suggestions made by <b>Angelika Langer</b> in her <a href=
 * "http://www.angelikalanger.com/Articles/JavaSolutions/SecretsOfEquals/Equals-2.html"
 * >article</a>. about her much complex full hierarchy traversing solution.
 * <p>
 * The solution is simple. Ensure that equality is compare always in one set direction. This way
 * symmetry contract is enforced. If compared class is subclass of this class, not same class but a
 * subclass, the <b>simply reverse</b> direction by calling equals() on the compared object.
 * Subclass has always better knowledge what and how it needs to be compared, it know about all
 * extra fields.
 * <p>
 * Restricted mixed type equals() - in this case we genuinely assume here that ColorPoint of any
 * color is not equal intrinsicly colorless Point even if they have got the same coordinates.
 * However any class extending Point but not adding value component should be equal having the same
 * coordinates. In that sense it is Liskov Substitution Principle compliant.
 * <p>
 * This is alternative implementation of Simple Reversing solution. It introduces a flag 'reversed'
 * preventing repeated reversion and therefore dropping the need to test if 'this' is subclass of
 * 'that' in every step. It is replaced by more simpler comparison against boolean flag. It is more
 * performant variant but it <b>requires a helper method</b>, or put this this way, two version of
 * equals(). Unless you tune for maximum performance, I would recommend the first flavour of 
 * Simple Reverse solution.
 */
public class A6EqualsAndInheritanceSimpleReversingSolutionAlt {

	@Test
	public void testEqualsSymmetryContractWithMixedTypes() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, 0);
		assertTrue("Broken symmetry", p.equals(cp) == cp.equals(p));
		// in symmetry test is not really important what equals returns, false or true
		// important is that is has to return SAME value for both directions
	}

	@Test
	public void testEqualsAndLiskovSubstitutionPrinciple() {
		ColorPoint cp = new ColorPoint(2, 3, 0);
		ColorPointDecorator cpd = new ColorPointDecorator(2, 3, 0);
		assertTrue("Broken substitution principle", cp.equals(cpd));
		assertTrue("Broken substitution principle", cpd.equals(cp));
	}

	@Test
	public void testEqualsForPoint() {
		Point p1 = new Point(2, 3);
		Point p2 = new Point(2, 3);
		Point p3 = new Point(1, 1);
		assertTrue("Points with same x and y must be equal", p1.equals(p2));
		assertFalse("Points with different x or y must be not be equal", p1.equals(p3));
	}

	@Test
	public void testEqualsForColorPoint() {
		ColorPoint cp1 = new ColorPoint(2, 3, 1);
		ColorPoint cp2 = new ColorPoint(2, 3, 1);
		ColorPoint cp3 = new ColorPoint(1, 1, 0);
		assertTrue("Color points with same x,y and color must be equal", cp1.equals(cp2));
		assertFalse("Color points with different x,y or color must be not be equal", cp1.equals(cp3));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(2, 3, 10)));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(10, 3, 1)));
	}

	@Test
	public void testReflexivityEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, 0);
		assertTrue("Broken reflexivity", cp.equals(cp));
		assertTrue("Broken reflexivity", p.equals(p));
	}

	@Test
	public void testMixedTypesWithAddedFieldsCannotEqual() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, 0);
		assertFalse("Mixed types with added fields cannot equal", p.equals(cp));
		assertFalse("Mixed types with added fields cannot equal", cp.equals(p));
		// Why this is not a Liskov Substitution Principle breach? 
		// we genuinely assume here that blue ColorPoint is not equal colorless Point even if they 
		// have got the same coordinates.
	}

	@Test
	public void testNullEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, 0);
		assertFalse("Broken null contract, equals(null) must return false", p.equals(null));
		assertFalse("Broken null contract, equals(null) must return false", cp.equals(null));
	}

	//
	// For demonstration purposes implementations of Point and ColorPoint are included in test class
	// 

	private static class Point {
		private final int x, y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object other) {
			return equals(other, false);
			// default equals is just for kickstarting flagged version of equals
		}

		// flagged version of equals
		public boolean equals(Object other, boolean reversed) {
			if (this == other)
				return true;
			if (!(other instanceof Point))
				return false;
			if (!reversed)
				// Here is the trick - if compared class is a subclass of this class then switch direction
				// flag 'reversed' prevents repeated reversion and therefore dropping the need to 
				// test if 'this' is subclass of 'that' in every step. Reversion happens only once.
				// In this variant is safe to reverse direction even when classes are same, flag prevents endless recursion.
				// Reversion can happen only once max.
				return ((Point) other).equals(this, true);
			Point otherPoint = (Point) other;
			if (x != otherPoint.x || y != otherPoint.y) // check fields  
				return false;
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPoint extends Point {
		private final int color;

		public ColorPoint(int x, int y, int color) {
			super(x, y);
			this.color = color;
		}

		// flagged version of equals
		@Override
		public boolean equals(Object other, boolean reversed) {
			if (this == other)
				return true;
			if (!(other instanceof ColorPoint))
				return false;
			if (!reversed)
				// Here is the trick - if compared class is a subclass of this class then switch direction 
				return ((Point) other).equals(this, true);
			ColorPoint otherColorPoint = (ColorPoint) other;
			if (color != otherColorPoint.color) // check fields
				return false;
			if (!super.equals(other, reversed)) // check superclass fields 
				return false;
			return true;
		}
		// hashCode() was removed for brevity
		// equals(object) is inherited
	}

	private static class ColorPointDecorator extends ColorPoint {
		// extending classes without any "value components" 
		// can simply inherit equals() and blindlyEquals() as they are from super class 

		public ColorPointDecorator(int x, int y, int color) {
			super(x, y, color);
		}
		// methods adding functionality removed for brevity
	}
}