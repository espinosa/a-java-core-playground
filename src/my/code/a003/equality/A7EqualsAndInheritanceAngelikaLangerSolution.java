package my.code.a003.equality;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Equals and inheritance - uneasy relationship. According to Joshua Bloch "there is no way to
 * extend an instantiable class and add a value component while preserving the equals contract"
 * (page 38, Effective Java, item 8). Joshua Bloch suggest as only way to use Composition over
 * Inheritance. But what if you have to stick Inheritance?
 * <p>
 * This class demonstrates <b>Angelika Langer solution</b> from <a href=
 * "http://www.angelikalanger.com/Articles/JavaSolutions/SecretsOfEquals/Equals-2.html" >see
 * here</a>.
 * <p>
 * Full mixed type equals() variant - in this case we genuinely assume that Point and ColorPoint can
 * be genuinely equal when (explicitly) colorless ColorPoint is equals to (intrinsicly) colorless
 * Point. This assume that every (extra) value component (color in ColorPoint) has default value
 * (colorless color) making comparing subclass to superclass possible even when subclass have extra
 * value components.
 * <p>
 * This solution is fully and unarguably and 100% Liskov Substitution Principle compliant.
 */
public class A7EqualsAndInheritanceAngelikaLangerSolution {

	@Test
	public void testEqualsSymmetryContractWithMixedTypes() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertTrue("Broken symmetry", p.equals(cp) == cp.equals(p));
		// in symmetry test is not really important what equals returns, false or true
		// important is that is has to return SAME value for both directions
	}

	@Test
	public void testMixedTypesWithAddedFieldsAreConsideredEqualIfTheyContainDefaultValues() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		// We genuinely assume that Point and ColorPoint can be genuinely equal when (explicitly) colorless 
		// ColorPoint is equals to (intrinsicly) colorless Point. Verify that. 
		assertTrue("Mixed types with added fields ARE consider equal for colorless points", p.equals(cp));
		assertTrue("Mixed types with added fields ARE considered equal for colorless points", cp.equals(p));
		// Verify that ColorPoint with non-default color is not equal to Point even when they have same coordinates
		ColorPoint cp2 = new ColorPoint(2, 3, Color.BLUE);
		assertFalse("Mixed types with added fields NOT consider equal for color points", p.equals(cp2));
		assertFalse("Mixed types with added fields NOT considered equal for color points", cp2.equals(p));
	}

	@Test
	public void testEqualsAndLiskovSubstitutionPrinciple() {
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		ColorPointDecorator cpd = new ColorPointDecorator(2, 3, Color.TRANSPARENT);
		assertTrue("Broken substitution principle", cp.equals(cpd));
		assertTrue("Broken substitution principle", cpd.equals(cp));
	}

	@Test
	public void testEqualsForPoint() {
		Point p1 = new Point(2, 3);
		Point p2 = new Point(2, 3);
		Point p3 = new Point(1, 1);
		assertTrue("Points with same x and y must be equal", p1.equals(p2));
		assertFalse("Points with different x or y must be not be equal", p1.equals(p3));
	}

	@Test
	public void testEqualsForColorPoint() {
		ColorPoint cp1 = new ColorPoint(2, 3, Color.BLUE);
		ColorPoint cp2 = new ColorPoint(2, 3, Color.BLUE);
		ColorPoint cp3 = new ColorPoint(1, 1, Color.TRANSPARENT);
		assertTrue("Color points with same x,y and color must be equal", cp1.equals(cp2));
		assertFalse("Color points with different x,y or color must be not be equal", cp1.equals(cp3));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(2, 3, Color.RED)));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(10, 3, Color.BLUE)));
	}

	@Test
	public void testReflexivityEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertTrue("Broken reflexivity", cp.equals(cp));
		assertTrue("Broken reflexivity", p.equals(p));
	}
	
	@Test
	public void testNullEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertFalse("Broken null contract, equals(null) must return false", p.equals(null));
		assertFalse("Broken null contract, equals(null) must return false", cp.equals(null));
	}

	//
	// For demonstration purposes implementations of Point and ColorPoint are included in test class
	// 

	private static class Point {
		private final int x, y;
		private final static int DEFAULT_X = 0;
		private final static int DEFAULT_Y = 0;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object other) {
			if (other == this)
				return true;
			if (other == null)
				return false;
			if (!(other instanceof Point))
				return false;
			return navigateClassHierarchy(other, false);
		}

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof Point && !reverseOrder) {
				// reverse order 
				return ((Point) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// since we are the root, succeed
				return true;
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof Point) {
				if (x != ((Point) other).x || y != ((Point) other).y)
					return false;
			} else { // higher type, check defaults
				if (x != DEFAULT_X || y != DEFAULT_Y)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPoint extends Point {
		private final Color color;
		private final static Color DEFAULT_COLOR = Color.TRANSPARENT;

		public ColorPoint(int x, int y, Color color) {
			super(x, y);
			this.color = color;
		}

		// equals() is inherited from root class Point
		// this also include comparing instance type against root type Point not local class type ColorPoint   

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof ColorPoint && !reverseOrder) {
				// reverse order 
				return ((ColorPoint) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// pass the buck up
				return super.navigateClassHierarchy(other, reverseOrder);
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof ColorPoint) {
				if (color != ((ColorPoint) other).color)
					return false;
			} else { // higher type, check defaults
				if (color != DEFAULT_COLOR)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPointDecorator extends ColorPoint {
		// extending classes without any "value components" 
		// can simply inherit equals() and blindlyEquals() as they are from super class 

		public ColorPointDecorator(int x, int y, Color color) {
			super(x, y, color);
		}
		// methods adding functionality removed for brevity
	}

	private enum Color {
		TRANSPARENT, BLUE, RED;
	}
}