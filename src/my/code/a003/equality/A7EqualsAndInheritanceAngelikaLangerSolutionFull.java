package my.code.a003.equality;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Equals and inheritance - uneasy relationship. According to Joshua Bloch "there is no way to
 * extend an instantiable class and add a value component while preserving the equals contract"
 * (page 38, Effective Java, item 8). Joshua Bloch suggest as only way to use Composition over
 * Inheritance. But what if you have to stick Inheritance?
 * <p>
 * This class demonstrates <b>Angelika Langer solution</b> from <a href=
 * "http://www.angelikalanger.com/Articles/JavaSolutions/SecretsOfEquals/Equals-2.html" >see
 * here</a>.
 * <p>
 * This example demonstrates full power of Angelika Langer solution by adding extra classes Point3D
 * and ColorPoint3D. They form two separate hierarchy branches and despite this extra complication
 * we are able to match them equal when common fields are equal and specific fields having default
 * values. That is ColorPoint3D(2,3,0,Color.TRANSPARENT) is equal ColorPoint(2,3,Color.TRANSPARENT)
 * or Point(2,3) or Point3D(2,3,0).
 * <p>
 * Full mixed type equals() variant - in this case we genuinely assume that Point and ColorPoint can
 * be genuinely equal when (explicitly) colorless ColorPoint is equals to (intrinsicly) colorless
 * Point. This assume that every (extra) value component (color in ColorPoint) has default value
 * (colorless color) making comparing subclass to superclass possible even when subclass have extra
 * value components.
 * <p>
 * This solution is fully and unarguably and 100% Liskov Substitution Principle compliant.
 */
public class A7EqualsAndInheritanceAngelikaLangerSolutionFull {

	@Test
	public void testEqualsSymmetryContractWithMixedTypes() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertTrue("Broken symmetry", p.equals(cp) == cp.equals(p));
		// in symmetry test is not really important what equals returns, false or true
		// important is that is has to return SAME value for both directions

		Point3D p3d = new Point3D(2, 3, 0);
		ColorPoint3D cp3d = new ColorPoint3D(2, 3, 0, Color.TRANSPARENT);
		assertTrue("Broken symmetry", p3d.equals(cp3d) == cp3d.equals(p3d));
		assertTrue("Broken symmetry", cp.equals(cp3d) == cp3d.equals(cp));
		assertTrue("Broken symmetry", p.equals(cp3d) == cp3d.equals(p));
		assertTrue("Broken symmetry", cp.equals(p3d) == p3d.equals(cp));
		assertTrue("Broken symmetry", p.equals(p3d) == p3d.equals(p));
	}

	@Test
	public void testMixedTypesWithAddedFieldsAreConsideredEqualIfTheyContainDefaultValues() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		// We genuinely assume that Point and ColorPoint can be genuinely equal when (explicitly) colorless 
		// ColorPoint is equals to (intrinsicly) colorless Point. Verify that. 
		assertTrue("Mixed types with added fields ARE considered equal for colorless points", p.equals(cp));
		assertTrue("Mixed types with added fields ARE considered equal for colorless points", cp.equals(p));
		// Verify that ColorPoint with non-default color is not equal to Point even when they have same coordinates
		ColorPoint cp2 = new ColorPoint(2, 3, Color.BLUE);
		assertFalse("Mixed types with added fields NOT consider equal for color points", p.equals(cp2));
		assertFalse("Mixed types with added fields NOT considered equal for color points", cp2.equals(p));
		
		Point3D point3d = new Point3D(2, 3, 0);
		ColorPoint3D transpPoint3d = new ColorPoint3D(2, 3, 0, Color.TRANSPARENT);
		assertTrue("Mixed types with added fields ARE considered equal for colorless points with z=0", point3d.equals(transpPoint3d));
		assertTrue("Mixed types with added fields ARE considered equal for colorless points with z=0", transpPoint3d.equals(point3d));
		// Verify that ColorPoint with non-default color is not equal to Point even when they have same coordinates
		ColorPoint3D bluePoint3d = new ColorPoint3D(2, 3, 0, Color.BLUE);
		ColorPoint3D highPoint3d = new ColorPoint3D(2, 3, 100, Color.TRANSPARENT);
		assertFalse("Points are not equals when they have different color", bluePoint3d.equals(p));
		assertFalse("Points are not equals when they have different color", bluePoint3d.equals(point3d));
		assertFalse("Points are not equals when they have different Z coordinate", highPoint3d.equals(p));
		assertFalse("Points are not equals when they have different Z coordinate", highPoint3d.equals(point3d));
	}

	@Test
	public void testEqualsAndLiskovSubstitutionPrinciple() {
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		ColorPointDecorator cpd = new ColorPointDecorator(2, 3, Color.TRANSPARENT);
		assertTrue("Broken substitution principle", cp.equals(cpd));
		assertTrue("Broken substitution principle", cpd.equals(cp));
	}

	@Test
	public void testEqualsForPoint() {
		Point p1 = new Point(2, 3);
		Point p2 = new Point(2, 3);
		Point p3 = new Point(1, 1);
		assertTrue("Points with same x and y must be equal", p1.equals(p2));
		assertFalse("Points with different x or y must be not be equal", p1.equals(p3));
	}

	@Test
	public void testEqualsForColorPoint() {
		ColorPoint cp1 = new ColorPoint(2, 3, Color.BLUE);
		ColorPoint cp2 = new ColorPoint(2, 3, Color.BLUE);
		ColorPoint cp3 = new ColorPoint(1, 1, Color.TRANSPARENT);
		assertTrue("Color points with same x,y and color must be equal", cp1.equals(cp2));
		assertFalse("Color points with different x,y or color must be not be equal", cp1.equals(cp3));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(2, 3, Color.RED)));
		assertFalse("Color points with different x,y or color must be not be equal",
			cp1.equals(new ColorPoint(10, 3, Color.BLUE)));
	}
	
	@Test
	public void testEqualsForPoint3D() {
		Point3D p1 = new Point3D(2, 3, 4);
		Point3D p2 = new Point3D(2, 3, 4);
		Point3D p3 = new Point3D(1, 1, 1);
		assertTrue("Points with same x and y and z must be equal", p1.equals(p2));
		assertFalse("Points with different x or y or z must be not be equal", p1.equals(p3));
	}

	@Test
	public void testEqualsForColorPoint3D() {
		ColorPoint3D cp1 = new ColorPoint3D(2, 3, 4, Color.BLUE);
		ColorPoint3D cp2 = new ColorPoint3D(2, 3, 4, Color.BLUE);
		ColorPoint3D cp3 = new ColorPoint3D(1, 1, 1, Color.TRANSPARENT);
		assertTrue("Color points with same x,y and color must be equal", cp1.equals(cp2));
		assertFalse("Color points with different x,y,z or color must be not be equal", cp1.equals(cp3));
		assertFalse("Color points with different x,y,z or color must be not be equal",
			cp1.equals(new ColorPoint3D(2, 3, 4, Color.RED)));
		assertFalse("Color points with different x, y, z or color must be not be equal",
			cp1.equals(new ColorPoint3D(10, 3, 4, Color.BLUE)));
		assertFalse("Color points with different x, y, z or color must be not be equal",
			cp1.equals(new ColorPoint3D(2, 3, 100, Color.BLUE)));
	}

	@Test
	public void testReflexivityEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertTrue("Broken reflexivity", cp.equals(cp));
		assertTrue("Broken reflexivity", p.equals(p));
	}
	
	@Test
	public void testNullEqualsContract() {
		Point p = new Point(2, 3);
		ColorPoint cp = new ColorPoint(2, 3, Color.TRANSPARENT);
		assertFalse("Broken null contract, equals(null) must return false", p.equals(null));
		assertFalse("Broken null contract, equals(null) must return false", cp.equals(null));
	}

	//
	// For demonstration purposes implementations of Point and ColorPoint are included in test class
	// 

	private static class Point {
		private final int x, y;
		private final static int DEFAULT_X = 0;
		private final static int DEFAULT_Y = 0;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object other) {
			if (other == this)
				return true;
			if (other == null)
				return false;
			if (!(other instanceof Point))
				return false;
			return navigateClassHierarchy(other, false);
		}

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof Point && !reverseOrder) {
				// reverse order 
				return ((Point) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// since we are the root, succeed
				return true;
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof Point) {
				if (x != ((Point) other).x || y != ((Point) other).y)
					return false;
			} else { // higher type, check defaults
				if (x != DEFAULT_X || y != DEFAULT_Y)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPoint extends Point {
		private final Color color;
		private final static Color DEFAULT_COLOR = Color.TRANSPARENT;

		public ColorPoint(int x, int y, Color color) {
			super(x, y);
			this.color = color;
		}

		// equals() is inherited from root class Point
		// this also include comparing instance type against root type Point not local class type ColorPoint   

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof ColorPoint && !reverseOrder) {
				// reverse order 
				return ((ColorPoint) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// pass the buck up
				return super.navigateClassHierarchy(other, reverseOrder);
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof ColorPoint) {
				if (color != ((ColorPoint) other).color)
					return false;
			} else { // higher type, check defaults
				if (color != DEFAULT_COLOR)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPointDecorator extends ColorPoint {
		// extending classes without any "value components" 
		// can simply inherit equals() and blindlyEquals() as they are from super class 

		public ColorPointDecorator(int x, int y, Color color) {
			super(x, y, color);
		}
		// methods adding functionality removed for brevity
	}

	private enum Color {
		TRANSPARENT, BLUE, RED;
	}

	private static class Point3D extends Point {
		private final int z;
		private final static int DEFAULT_Z = 0;

		public Point3D(int x, int y, int z) {
			super(x, y);
			this.z = z;
		}

		// equals() is inherited from root class Point
		// this also include comparing instance type against root type Point not local class type Point3D   

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof Point3D && !reverseOrder) {
				// reverse order 
				return ((Point3D) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// pass the buck up
				return super.navigateClassHierarchy(other, reverseOrder);
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof Point3D) {
				if (z != ((Point3D) other).z)
					return false;
			} else { // higher type, check defaults
				if (z != DEFAULT_Z)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}

	private static class ColorPoint3D extends ColorPoint {
		private final int z;
		private final static int DEFAULT_Z = 0;

		public ColorPoint3D(int x, int y, int z, Color color) {
			super(x, y, color);
			this.z = z;
		}

		// equals() is inherited from root class Point
		// this also include comparing instance type against root type Point not local class type ColorPoint3D   

		protected boolean navigateClassHierarchy(Object other, boolean reverseOrder) {
			if (other instanceof ColorPoint3D && !reverseOrder) {
				// reverse order 
				return ((ColorPoint3D) other).navigateClassHierarchy(this, true);
			} else {
				if (!compareFields(other))
					return false;
				// pass the buck up
				return super.navigateClassHierarchy(other, reverseOrder);
			}
		}

		private boolean compareFields(Object other) {
			if (other instanceof ColorPoint3D) {
				if (z != ((ColorPoint3D) other).z)
					return false;
			} else { // higher type, check defaults
				if (z != DEFAULT_Z)
					return false;
			}
			return true;
		}
		// hashCode() was removed for brevity
	}
}