Variation of previous assessment:

TaskManager implementation using Java5 concurrency package classes, LinkedBlockingQueue precisely. 
It demonstrates how the TaskManager core methods were made much simpler.
Features: no synchronisation keywords on submit() and take() 

@author Jan Uhlir (2011 April)
