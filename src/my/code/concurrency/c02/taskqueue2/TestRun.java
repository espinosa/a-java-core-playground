package my.code.concurrency.c02.taskqueue2;

import my.code.concurrency.c02.taskqueue.common.PrintStatus;
import my.code.concurrency.c02.taskqueue.common.TestClient;


/**
 * Simulate traffic, simulate some random client calls (random gaps).
 * 
 * Expected print output sample:
 * <pre>
 * Requests total: 23 waiting: 0; workers progress: [50.0%, 30.0%, 50.0%, 90.0%, 90.0%]
 * Requests total: 24 waiting: 0; workers progress: [60.0%, 40.0%, 60.0%, 90.0%, 0.0%]
 * Requests total: 26 waiting: 1; workers progress: [70.0%, 50.0%, 70.0%, 0.0%, 10.0%]
 * Requests total: 28 waiting: 3; workers progress: [80.0%, 60.0%, 80.0%, 10.0%, 20.0%]
 * Requests total: 28 waiting: 3; workers progress: [90.0%, 70.0%, 90.0%, 20.0%, 30.0%]
 * Requests total: 29 waiting: 2; workers progress: [0.0%, 80.0%, 0.0%, 30.0%, 40.0%]
 * Requests total: 30 waiting: 3; workers progress: [10.0%, 90.0%, 10.0%, 40.0%, 50.0%]
 * Requests total: 31 waiting: 3; workers progress: [20.0%, 0.0%, 20.0%, 50.0%, 60.0%]
 * </pre>
 * 
 * @see TestClient
 * @author espinosa (finished 4.4.2011)
 */
public class TestRun {
	
	/** number of test clients sending random requests */
	public static final int NUMBER_OF_CLIENTS = 10;

	/** 
	 * Set client "activity".
	 * The lower the client is, the more requests client sends. 
	 * Clients are simulated to send requests randomly so this is the base for the 
	 * randomly generated number.
	 */
	public static final int MAX_CLENT_IDLE_TIME = 5000;
	
	/** 
	 * Fixed (or maximal) number of TM Workers (pool size)
	 * The higher the number the better performance - just because because our workers do a lot of waiting
	 * this is not a general implication! 
	 */ 
	public static final int NUMBER_OF_WORKERS = 5;
	
	/** fixed (or maximal) number of TM Workers (pool size) */ 
	public static final int STATUS_PRINTER_INTERVAL = 100;
	
 
	
	/** TM instance. Initialise  task manager as singleton? */
	public static final TaskManager2 taskManager = new TaskManager2(NUMBER_OF_WORKERS);

	/** Main method */
	public static void main(String[] args) {
		System.out.println("start");
		
		// instantiate Task Manager. TM handles its pool of workers.
		taskManager.start();

		// Initialise mock clients
		for (int i=0; i < NUMBER_OF_CLIENTS ; i++) {
			new Thread(new TestClient(taskManager, "Client"+i, MAX_CLENT_IDLE_TIME)).start();
		}

		// ..let the tread print on console..
		PrintStatus statusPrinter = new PrintStatus(taskManager, STATUS_PRINTER_INTERVAL);
		new Thread(statusPrinter).start();
	}

}
