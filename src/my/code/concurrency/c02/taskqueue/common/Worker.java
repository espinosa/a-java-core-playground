package my.code.concurrency.c02.taskqueue.common;


public class Worker implements Runnable {

	/**
	 * Job queue reference
	 */
	private ITaskManager taskManager;
	
	/**
	 * Worker Thread indentification.
	 * Alternative: Thread.currentThread().getId()
	 */
	@SuppressWarnings("unused")
	private int workerNumber;
	
	
	public int progress;
	
	/** 
	 * Does this has to be volatile?
	 */
	private volatile boolean stop = false;
	
	public Worker(ITaskManager taskManager, int workerNumber) {
		this.taskManager = taskManager;
		this.workerNumber = workerNumber;
	}
	
	/**
	 * Main method. Take a request from a central queue (TM) and process it.
	 * In case queue is empty - wait.
	 * Do not use lame test - sleep - test loop but a proper thread blocking.
	 */
	@Override
	public void run() {
		while (!stop) {
			// ask for a job, possibly wait here until some is available
			Request request = taskManager.take();
			if (request != null) {
				// Not sure if NULL can happen. Spurious wake-up (?)
				processRequest(request);
			}
		}
	}
	
	/**
	 * Thread control method - stop thread, finish worker gracefuly.
	 */
	public void stop() {
		stop = true;
	}
	
	/**
	 * In real life this would be a delegate to some service/EJB/.. Here we just
	 * for test sake print worker thread number.
	 * <p>
	 * It has not to be synchronised because is naturally thread safe: (1) local
	 * variables only, (2) console printing has its own synchronisation (I hope)
	 * 
	 * @param request
	 */
	protected void processRequest(Request request) {
		// do something
		for (int i=0; i < 10; i++) {
			//System.out.print("[" + workerNumber + "," + i + "]");
			progress = i;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { 
				Thread.currentThread().interrupt();
			}
		}
	}
	
	/**
	 * Get request processing progress as percentage
	 * @return 0 - 100, meaning 0% - 100%
	 */
	public double getProgress() {
		return progress / 10d * 100;
	}

}
