package my.code.concurrency.c02.taskqueue.common;

/**
 * Task manager interface. Task manager receives client requests, handles
 * request queue, assigns worker threads to requests depending on their
 * availability.
 * 
 * @author espinosa
 */
public interface ITaskManager {

	/**
	 * Called by client issuing request. Request will be processes when some
	 * worker thread is available, so not necessary immediately. A non-blocking
	 * call.
	 * 
	 * @param request
	 *            request submitted by client. It is supposed to contain some
	 *            data or instructions.
	 */
	public void submit(Request request);

	/**
	 * Called by worker threads. Take a job from a job queue if there is any,
	 * otherwise wait (blocking call).
	 * 
	 * @return request from a queue
	 */
	public Request take();

	/**
	 * @return queue status for monitoring purposes
	 */
	public TaskManagerStatus getStatus();
}
