package my.code.concurrency.c02.taskqueue.common;

public class TaskManagerStatus {
	public int queueLength;
	public double[] workersProgress;
	public long totalReceived;
}
