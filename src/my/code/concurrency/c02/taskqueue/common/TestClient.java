package my.code.concurrency.c02.taskqueue.common;

import java.util.Random;

import my.code.concurrency.c02.taskqueue2.TestRun;

/**
 * Simulation of client. Send requests in random intervals. Run forever, until whole application is stopped.
 * @see TestRun
 */
public class TestClient implements Runnable {
	public static final Random random = new Random();
	private ITaskManager taskManager;
	private String clientName;
	private int maxClientIdleTime;
	

	public TestClient(ITaskManager taskManager, String clientName, int maxClientIdleTime) {
		this.taskManager = taskManager;
		this.clientName = clientName;
		this.maxClientIdleTime = maxClientIdleTime;
	}

	@Override
	public void run() {
		int requestNumber = 1;
		// run forever 
		while (true) {
			try {
				Request request = new Request(clientName + ":" + requestNumber++);
				taskManager.submit(request);
				//System.out.println("Request " + request + " submitted");
				
				Thread.sleep(random.nextInt(maxClientIdleTime));
			} catch (InterruptedException e) { 
				// Allegedly this is Correct interrupted exception handling
				// if you cannot to propagate the exception (Runnable run() signature from interface limitation)
				Thread.currentThread().interrupt();
			}
		}
	}
}

