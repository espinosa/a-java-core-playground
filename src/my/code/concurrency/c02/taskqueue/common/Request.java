package my.code.concurrency.c02.taskqueue.common;

/**
 * Simplified request 
 * 
 * @author espinosa
 */
public class Request {
	public String requestName;

	public Request(String requestName) {
		this.requestName = requestName;
	}

	@Override
	public String toString() {
		return requestName;
	}
}
