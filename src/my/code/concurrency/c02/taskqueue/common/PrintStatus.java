package my.code.concurrency.c02.taskqueue.common;


/**
 * Print status of task queue and its workers in short intervals. Run as daemon thread.
 * @author espinosa
 */
public class PrintStatus implements Runnable {
	
	public ITaskManager taskManager; 
	public long interval;
	
	public PrintStatus(ITaskManager taskManager, long interval) {
		this.taskManager = taskManager;
		this.interval = interval;
	}

	@Override
	public void run() {
		while (true) {
			System.out.println(statusAsString());
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	public String statusAsString() {
		TaskManagerStatus info = taskManager.getStatus();
		StringBuilder sb = new StringBuilder();
		sb.append("Requests total: ");
		sb.append(info.totalReceived);
		sb.append(" waiting: ");
		sb.append(info.queueLength);
		sb.append("; workers progress: [");
		for (int i = 0; i < info.workersProgress.length; i++) {
			sb.append(info.workersProgress[i]);
			sb.append("%");
			if (i < (info.workersProgress.length - 1)) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
}
