Assessment:

Create a frame of a system to deal with random client requests, where requests from clients come randomly, 
possibly several at a time, and gaps with no clients requests, no work at all. Processing of the request must be fully 
asynchronous, client does not have to wait for result.  

As simplification, suppose no callback feature required so client is not informed when and that request was finished.

Build a working demonstration of the implementation with:
- settable number of clients 
- simulate random nature of client requests (random gaps between clients, settable maximum length of the gap)
- settable number of worker thread (should be > 2 to demonstrate concurrency features :)
- periondically inform user about task queue status. Monitor queue status.
  Print cumulative number of requests processed, number of waiting to be processed requests (to monitor performance), status of processing (percentage?) 
  for each (active?) worker.  
  This is the only required visible output from the demo run.
- Dummy request and request processing implementation only (but not just empty method, something what makes CPU busy a bit)    

@author Jan Uhlir (2011 January - April)
