package my.code.concurrency.c05.tlab;

/**
 * Never ending mini program for concurrent object creation on heap.<br>
 * Good to test garbage collection and TLAB presence.<br>
 * See: http://stackoverflow.com/questions/25512146/how-jvm-ensure-thread-safety-of-memory-allocation-for-a-new-object
 * <p>
 * Run with VM parameter -XX:+PrintTLAB<br>
 * For details about JVM and Thread Local Allocation Buffers (TLAB) see: https://blogs.oracle.com/jonthecollector/entry/the_real_thing<br>
 * Ideally run from command line, for example like this from Eclipse project bin dir:<br>
 * <code>
 * bin$ java -XX:+PrintTLAB -XX:+UseSerialGC my.code.concurrency.c05.tlab.CreateObjectsInThreadsRun
 * </code>
 * 
 * @author espinosa
 */
public class CreateObjectsInThreadsRun {
	
	public static void main(String[] args) {
		System.out.println("Never ending program for concurrent object creation on heap - start");
		
		// create pressure on GC and heap to print some interesting reports on GC and TLAB activity
		for (int i=0; i < 5; i++) {
			new Thread(new Runnable() {
				Object[] tempRetainer = new Object[10000];
				int p = 0;
				public void run() {
					while (true) {
						tempRetainer[p++] = new DummyObject();
						if (p >= tempRetainer.length) p = 0;
					}
				}
			}).start();
		}
		
		// periodically print used memory size
		new Thread(new Runnable() {
	        int mb = 1024*1024;
	        Runtime runtime = Runtime.getRuntime();
			public void run() {
				while (true) {
			        System.out.println("Used Memory:"
			            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {}
				}
			}
		}).start();
	}
	
	public static class DummyObject {
		byte[] a = new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}; 
	}
}
