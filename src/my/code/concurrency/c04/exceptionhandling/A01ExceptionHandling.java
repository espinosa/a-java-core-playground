package my.code.concurrency.c04.exceptionhandling;

import java.util.Random;

/**
 * Exercise: what happens when non-main thread throws exception.
 * Use classic Thread initialization, not thread pool from Executors.
 * Assume main thread is expected to running for ever. 
 */
public class A01ExceptionHandling {
	private final static int NUM_THREADS = 10; 
	private final Random rgen = new Random();

	public void run() {
		// start worker threads		
		Thread naughtyThread = new Thread(new ExceptionThrowingWorker('X'));
		for (int i = 0; i < NUM_THREADS; i++) {
			if (i!=5) {
				new Thread(new NormalWorker(i)).start();
			}
			else {
				naughtyThread.start();
			}
		}
		// loop for ever
		while (true) {
			try {
				Thread.sleep(1000);
				System.out.println("\nNaughty thread status: " + naughtyThread.getState());
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static void main(String[] args) {
		new A01ExceptionHandling().run();
	}
	
	//------------------------------------------------ worker threads impl --------------
	
	class NormalWorker implements Runnable {
		private final int threadNumber;
		private final Random rgen = new Random();

		NormalWorker(int letter) {
			this.threadNumber = letter;
		}

		@Override
		public void run() {
			while (true) {
				System.out.print(threadNumber);
				if (rgen.nextInt(9) == 0) System.out.print("\n");
				try {
					Thread.sleep(rgen.nextInt(9) * 100);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	class ExceptionThrowingWorker implements Runnable {
		private final char threadNumber;
		private final Random rgen = new Random();

		ExceptionThrowingWorker(char threadNumber) {
			this.threadNumber = threadNumber;
		}

		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				System.out.print(threadNumber);
				try {
					Thread.sleep(rgen.nextInt(9) * 100);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			throw new RuntimeException("Exception in worker thread!");
		}
	}
}

//Sample output:
//	1304
//	32X64789
//	71
//	7
//	77
//	9634X820219
//	7
//	3
//	Naughty thread status: TIMED_WAITING
//	607724X66188384
//	93072
//	Naughty thread status: TIMED_WAITING
//	X363X971
//	9840713
//	X26489
//	Naughty thread status: TIMED_WAITING
//	68783X10488269931
//	Naughty thread status: TIMED_WAITING
//	6
//	743X609
//	6X698862963
//	710707
//	4X29
//	Naughty thread status: TIMED_WAITING
//	783
//	3
//	Exception in thread "Thread-0" 8
//	java.lang.RuntimeException: Exception in worker thread!
//		at my.code.concurrency.c04.A01ExceptionHandling$ExceptionThrowingWorker.run(A01ExceptionHandling.java:81)
//		at java.lang.Thread.run(Thread.java:722)
//	4133760867294380
//	011
//	7
//	Naughty thread status: TERMINATED
//	944228
//	6378940891632
//	076
//	Naughty thread status: TERMINATED
//	28399
//	1460
//	0634187
//	93294
//	Naughty thread status: TERMINATED
//	6101

