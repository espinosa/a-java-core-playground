package my.code.concurrency.c04.exceptionhandling;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Exercise: what happens when non-main thread throws exception.<br>
 * Use fixed thread pool from Executors.<br>
 * Assume main thread is expected to running for ever.<br>
 * <br>
 * Side question: Will the pool be N-1 after the failure?<br>
 */
public class A02ExceptionHandling {
	private final static int NUM_THREADS = 10;
	private static final ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
	private final Random rgen = new Random();

	public void run() {
		// start worker threads
		for (int i = 0; i < NUM_THREADS; i++) {
			if (i != 5)
				pool.execute(new NormalWorker(i));
			else
				pool.execute(new ExceptionThrowingWorker(i));
		}
		// loop for ever
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public static void main(String[] args) {
		new A02ExceptionHandling().run();
	}
}

class NormalWorker implements Runnable {
	private final int threadNumber;
	private final Random rgen = new Random();

	NormalWorker(int letter) {
		this.threadNumber = letter;
	}

	@Override
	public void run() {
		while (true) {
			System.out.print(threadNumber);
			if (rgen.nextInt(9) == 0)
				System.out.print("\n");
			try {
				Thread.sleep(rgen.nextInt(9) * 100);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}

class ExceptionThrowingWorker implements Runnable {
	private final int threadNumber;
	private final Random rgen = new Random();

	ExceptionThrowingWorker(int threadNumber) {
		this.threadNumber = threadNumber;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.print(threadNumber);
			try {
				Thread.sleep(rgen.nextInt(9) * 100);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		throw new RuntimeException("Exception in worker thread!");
	}
}

//Expected output (example):
//0231456
//7789524
//2
//0566830110
//82798488059632715168491
//
//400933602
//5860714932057316
//822482
//19
//58017
//304263584
//7976
//815
//9127660
//319184011148
//Exception in thread "pool-1-thread-6" java.lang.RuntimeException: Exception in worker thread!
//	at my.code.concurrency.c04.ExceptionThrowingWorker.run(A02ExceptionHandling.java:73)
//	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
//	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
//	at java.lang.Thread.run(Thread.java:722)
//201677
//8113324393713422966493333
//078009300
//81
//6
//134427440639624810737629
//44317
//38190777613243811122978486006
//984
//23627171
//6
//161134308
//9
//8862284
//979412069
//13
//47686687166326970
//7
