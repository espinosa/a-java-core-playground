package my.code.concurrency.c04.stopthread;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Demonstrate that timeouting Future/Callable is not stop underlying worker thread 
 * if the underlying worker implementation is not build to react to interruption. 
 * PrintNumberWoker will print letter 'A' for ever
 */
public class A01TryToStopThreadUsingFutureTimeout {
	private final ExecutorService pool = Executors.newFixedThreadPool(1);

	public void run() throws InterruptedException, ExecutionException {
		Future<DummyResult> future = pool.submit(new PrintNumberWoker('A'));
		try {
			future.get(10, TimeUnit.SECONDS); 
			// Ignore result of the Future, 
			// point of this call is to naively to timeout worker thread
			// Main thread stops here until the timeout, like with Thread#join()
		} catch (TimeoutException te) {
			System.out.println();
			System.out.println("PrintNumberWoker was timeouted, TimeoutException thrown");
			// exception is thrown but PrintNumberWoker keeps running, printing the letter A for ever
			System.out.println("Future is done: " + future.isDone());
			System.out.println("Future is cancelled: " + future.isCancelled());
		}
		// PrintNumberWoker will continue in printing letter A even after main thread 
		// finishes (because it is not daemon thread)
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		new A01TryToStopThreadUsingFutureTimeout().run();
	}

	static class PrintNumberWoker implements Callable<DummyResult> {
		private final char letter;
		private final Random rgen = new Random();

		PrintNumberWoker(char letter) {
			this.letter = letter;
		}

		@Override
		public DummyResult call() {
			while (true) {
				System.out.print(letter);
				if (rgen.nextInt(30) == 0)
					System.out.print("\n");
				try {
					Thread.sleep(rgen.nextInt(9) * 100);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					// Timeout throws TimeoutException, not InterruptedException!
					// Execution (normally) will not reach this block.
					// So we can leave it empty here or use this standard implementation
					// neither should have impact on the result. 
					// ..unless rare chance of getting InterruptedException from some other source
				}
			}
		}
	}

	static class DummyResult {

	}
}
//Expected output:
//	AAAAAAAAAAAAAAAAA
//	AAAAAA
//	PrintNumberWoker was timeouted, TimeoutException thrown
//	Future is done: false
//	Future is cancelled: false
//	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
//	AAAAAAAAAAAAAAAA
