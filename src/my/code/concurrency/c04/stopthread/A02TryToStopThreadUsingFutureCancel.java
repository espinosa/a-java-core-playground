package my.code.concurrency.c04.stopthread;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Demonstrate that timeouting Future/Callable is not stop underlying worker thread if the
 * underlying worker implementation is not build to react to interruption. PrintNumberWoker will
 * print letter 'A' for ever
 */
public class A02TryToStopThreadUsingFutureCancel {
	private final ExecutorService pool = Executors.newFixedThreadPool(1);

	public void run() throws InterruptedException, ExecutionException {
		Future<DummyResult> future = pool.submit(new PrintNumberWoker('B'));

		Thread.sleep(5000);
		// Ignore result of the Future, 
		// point of this call is to naively to timeout worker thread

		System.out.println();
		System.out.println("Try to cancel PrintNumberWoker after 5000ms waiting");
		future.cancel(true);

		// PrintNumberWoker keeps running, printing the letter A for ever
		System.out.println("\n" +
			"Future is done: " + future.isDone() + "\n" +
			"Future is cancelled: " + future.isCancelled());

		// PrintNumberWoker will continue in printing letter A even after Future is *formally*
		// flagged as Done==true and Cancelled==true as you will se from above printout.
		
		// PrintNumberWoker will continue in printing letter A even after main thread 
		// finishes (because it is not daemon thread)
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		new A02TryToStopThreadUsingFutureCancel().run();
	}

	static class PrintNumberWoker implements Callable<DummyResult> {
		private final char letter;
		private final Random rgen = new Random();

		PrintNumberWoker(char letter) {
			this.letter = letter;
		}

		@Override
		public DummyResult call() {
			while (true) {
				System.out.print(letter);
				if (rgen.nextInt(30) == 0)
					System.out.print("\n");
				try {
					Thread.sleep(rgen.nextInt(9) * 100);
				} catch (InterruptedException e) {
					// intentionally do nothing
					// this is a naughty thread, not reacting
					// virtually unstoppable!
				}
			}
		}
	}

	static class DummyResult {

	}
}
//Expected output:
//	BBBB
//	BBBBBBBBB
//	Try to cancel PrintNumberWoker after 5000ms waiting
//	B
//	Future is done: true
//	Future is cancelled: true
//	BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
//	BBBBBBBBBBBB
