This batch contains:

 - Demonstration of a naive attempt to stop thread using Future timeout
   and Future cancel. Worker thread keeps printing to stdout to signalize it
   is alive even when it was timeouted using Future#get(timeout). Same for Future#cancel() 
   It is common misconception how this think works on StackOverflow.com and this
   exercises are my reaction to them.

 - Exception propagation in non-main thread. Add demonstration that
   exception in non-main thread does not stop whole application even if it
   is not captured.