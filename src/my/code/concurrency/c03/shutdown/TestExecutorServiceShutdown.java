package my.code.concurrency.c03.shutdown;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Demonstrate importance of method {@link ExecutorService#shutdownNow()}
 * 
 * @author espinosa
 */
public class TestExecutorServiceShutdown {

	/** infinite printer task */
	static Runnable task1 = new Runnable() {
		@Override
		public void run() {
			while (true) {
				System.out.print("a");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					System.out.println();
					System.out.println("FooTask catched InterruptedException! ");
					break;
				}
			}
		}
	};

	public static void main(String[] args) throws Exception {
		ExecutorService exec = Executors.newSingleThreadExecutor();
		exec.execute(task1);
		Thread.sleep(1000);
		exec.shutdown(); // does nothing!
		exec.shutdownNow(); // invokes interrupted exception on threads
	}
}
