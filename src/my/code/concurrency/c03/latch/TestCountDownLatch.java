package my.code.concurrency.c03.latch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Demonstrate usage of {@link CountDownLatch} in keeping spawned thread on the
 * start line, starting them all at once and then make main thread waiting for
 * all spawned threads to finish and letting user know. I often used this
 * technique in concurrency behaviour tests.
 * 
 * Two count-down latches are needed for that - one to make worker threads
 * waiting, another one to make to wait for all Worker threads to finish. 
 * 
 * See: http://www.ibm.com/developerworks/java/library/j-5things5/index.html
 * 
 * @author espinosa
 */
public class TestCountDownLatch {

	public static final int N = 3; // number of concurrent threads

	public static String[] names = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l" };

	public static void main(String[] args) {
		System.out.println("start");
		CountDownLatch startLatch = new CountDownLatch(1); // latch to keep all spawned thread on "starting line"
		CountDownLatch finishLatch = new CountDownLatch(N); // latch to keep main thread for all N worker threads to finish
		
		// start the worker threads, but not really running
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for (int i = 0; i < N; i++) {
			executor.execute(new Worker(startLatch, finishLatch, names[i]));
		}
		
		startLatch.countDown(); // start the waiting N threads, make them run
		
		// make main thread to wait for all worker threads to finish
		try {
			finishLatch.await(); // wait until all finish
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		System.out.println();
		System.out.println("finish"); // wait until all finish ..then print "finish"

		// correct output with latch:
		// start
		// abcacbbacabcacbcabbacbcabaccba
		// finish

		// wrong output without latch:
		// start
		// finish
		// abcacbbacabcacbcabbacbcabaccba
		// ..main thread (usually) finishes sooner then spawned threads
	}
}
