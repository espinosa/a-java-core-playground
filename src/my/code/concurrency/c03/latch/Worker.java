package my.code.concurrency.c03.latch;

import java.util.concurrent.CountDownLatch;

public final class Worker implements Runnable {

	CountDownLatch startLatch;
	CountDownLatch finishLatch;
	String name;

	public Worker(CountDownLatch  startLatch, CountDownLatch  finishLatch, String name) {
		this.startLatch = startLatch;
		this.finishLatch = finishLatch;
		this.name = name;
	}

	/**
	 * Do something worker thread
	 */
	@Override
	public void run() {

		try {
			// wait for star signal
			startLatch.await();

			// do some work
			doSomeWork();

			// Signalize finish
			finishLatch.countDown();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Do some dummy activity to visibly demonstrate that this thread is active.
	 * Print letter 'B' to the standard output in 300ms interval.
	 */
	public void doSomeWork() {
		// do some work, print 'A'
		for (int i=0; i<10; i++) {
			System.out.print(name);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) { }
		}
	}
}
