package my.code.concurrency.c03.semaphor;

import java.util.concurrent.Semaphore;

/**
 * Simple test worker printing a short string (usually just a letter) as a proof of its activity.
 */
public final class Worker implements Runnable {

	Semaphore semaphore;
	String name;

	public Worker(Semaphore semaphore, String name) {
		this.semaphore = semaphore;
		this.name = name;
	}

	/**
	 * Do something worker thread
	 */
	@Override
	public void run() {
		try {
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} 
			// do some work
			doSomeWork();
		} finally {
			semaphore.release();
		}
	}

	/**
	 * Do some dummy activity to visibly demonstrate that this thread is active.
	 * Print worker name, usually just one letter, to the standard output in short intervals.
	 */
	public void doSomeWork() {
		// do some work, print 'A'
		for (int i=0; i<10; i++) {
			System.out.print(name);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) { }
		}
	}
}
