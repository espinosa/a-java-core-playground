package my.code.concurrency.c03.semaphor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Demonstrate throttling using {@link Semaphore} from Java Concurrent package.
 * Permit only subset of all triggered threads to operate on the same time. See:
 * http://www.ibm.com/developerworks/java/library/j-5things5/index.html
 * 
 * @author espinosa
 */
public class TestSemaphoreThrottling {

	public static final int TOTAL_THREADS_NUMBER = 5;

	public static final int PERMITTED_THREADS_NUMBER = 2;

	public static String[] names = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l" };

	public static void main(String[] args) {
		System.out.println("start");
		Semaphore semaphore = new Semaphore(PERMITTED_THREADS_NUMBER);

		ExecutorService executor = Executors.newFixedThreadPool(TOTAL_THREADS_NUMBER);
		for (int i = 0; i < TOTAL_THREADS_NUMBER; i++) {
			executor.execute(new Worker(semaphore, names[i]));
		}
		// you should see output like:
		// ababbaabbaabababababcddcdcdccdcdcddccddceeeeeeeeee
		// always just 2 letters (PERMITTED_THREADS_NUMBER) should be interleaving out of total 5 (PERMITTED_THREADS_NUMBER)
	}
}
