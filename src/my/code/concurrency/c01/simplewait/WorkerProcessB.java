package my.code.concurrency.c01.simplewait;

/**
 * Purpose of this demonstration is to make process B should wait until process
 * A is ready, even when they are started at the same time or B first, using
 * just pre-Java 5 tools - intrinsic Object locks and wait/notify.
 * 
 * @author Jan Uhlir
 */
public class WorkerProcessB implements Runnable {

	/**
	 * intrinsic lock holder. It must be object shared by all thread. So the
	 * lock holder cannot be 'this' :)
	 */
	private final Object lock;

	/**
	 * Signalling object, signal when one thread finishes so the other can start
	 * or proceed
	 */
	private final ProcessBus bus;

	public WorkerProcessB(Object lock, ProcessBus bus) {
		this.lock = lock;
		this.bus = bus;
	}

	/**
	 * Wait until first thread is finished and then resume.
	 */
	@Override
	public void run() {
		synchronized (lock) {
			// set status as finished as ready
			while (!bus.isFinished()) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
		doSomeWork();
	}

	/**
	 * Do some dummy activity to visibly demonstrate that this thread is active.
	 * Print letter 'B' to the standard output in 300ms interval.
	 */
	public void doSomeWork() {
		// do some work, print 'B'
		for (int i = 0; i < 10; i++) {
			System.out.print("B");
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
