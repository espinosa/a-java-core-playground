package my.code.concurrency.c01.simplewait;

/**
 * Purpose of this demonstration is to make process B should wait until process
 * A is ready, even when they are started at the same time or B first, using
 * just pre-Java 5 tools - intrinsic Object locks and wait/notify.
 * 
 * A shared 'token' class for signalling between threads, mark when one is
 * finished and other can continue, I picked the word 'bus' to denote
 * possibility of potentially several chained processes, connected by this
 * 'bus'.
 * 
 * This is not a thread safe object, it is shared between threads, so it has to
 * be used inside synchronized blocks
 * 
 * @author Jan Uhlir
 */
public class ProcessBus {
	private boolean finished = false;

	public boolean isFinished() {
		return finished;
	}

	public void setFinished() {
		this.finished = true;
	}
}
