Simple test example of 2 concurrent threads, both started at the same time, but ThreadB must wait for ThreadA to finish.
Use Java < 5 concurrency mechanism, demonstrate usage of: synchronized, wait(), notifyAll()

This is a simplified variant on Consumer-Producer topic. Just there is no real producing and consuming and so no intrinsic 
dependency between this test threads. 

@author Jan Uhlir (2011 January)
 
