package my.code.concurrency.c01.simplewait;

/**
 * Make thread (process) B wait orderly for thread (process) A to finish. Show
 * How to make a java thread wait for another threads finish.
 * <p>
 * Purpose of this demonstration is to make process B should wait until process
 * A is ready, even when they are started at the same time or B first, using
 * just pre-Java 5 tools - intrinsic Object locks and wait/notify.
 * <p>
 * Expected output:<br>
 * AAAAAAAAAABBBBBBBBBB
 * <p>
 * http://stackoverflow.com/questions/289434/how-to-make-a-java-thread-wait-for-
 * another-threads-output
 * 
 * @author Jan Uhlir
 */
public class TestRun {

	public static void main(String[] args) {
		final Object sharedLock = new Object();
		ProcessBus processBus = new ProcessBus();

		new Thread(new WorkerProcessB(sharedLock, processBus)).start();
		new Thread(new WorkerProcessA(sharedLock, processBus)).start();

		// should slowly (300ms a letter) print:
		// AAAAAAAAAABBBBBBBBBB
		// ..as opposite to something like ABAABBABABBABABAA without waiting
		// process B wait ordely for process A to finish
		// Printing A means thread A is active. Printing B menas thred B is active.
	}
}
