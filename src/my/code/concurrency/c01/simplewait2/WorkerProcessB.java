package my.code.concurrency.c01.simplewait2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Purpose of this demonstration is to make process B should wait until process
 * A is ready, even when they are started at the same time or B first, using
 * Java 5 concurrency package tools - Locks, Conditions and await/signal
 * 
 * @author Jan Uhlir
 */
public class WorkerProcessB implements Runnable {

	/**
	 * intrinsic lock holder. It must be object shared by all thread. So the
	 * lock holder cannot be 'this' :)
	 */
	private final Lock lock;

	/**
	 * Signalling object, signal when one thread finishes so the other can start
	 * or proceed
	 */
	private final Condition condition;

	public WorkerProcessB(Lock lock, Condition condition) {
		this.lock = lock;
		this.condition = condition;
	}

	@Override
	public void run() {
		//Lock must be set, otherwise you get: Exception in thread "Thread-1" java.lang.IllegalMonitorStateException
		lock.lock();
		try {
			condition.await();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		lock.unlock();
		doSomeWork();
	}

	/**
	 * Do some dummy activity to visibly demonstrate that this thread is active.
	 * Print letter 'B' to the standard output in 300ms interval.
	 */
	public void doSomeWork() {
		// do some work, print 'B'
		for (int i = 0; i < 10; i++) {
			System.out.print("B");
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
