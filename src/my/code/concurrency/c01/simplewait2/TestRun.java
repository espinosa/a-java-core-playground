package my.code.concurrency.c01.simplewait2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Purpose of this demonstration is to make process B should wait until process
 * A is ready, even when they are started at the same time or B first, using
 * Java 5 concurrency package tools - Locks, Conditions and wait/signal
 * <p>
 * Ensure that A is done before B.
 * <p>
 * Expected output:<br> 
 * AAAAAAAAAABBBBBBBBBB
 * <p>
 * See http://stackoverflow.com/questions/289434/how-to-make-a-java-thread-wait-for-
 * another-threads-output
 * 
 * @author Jan Uhlir
 */
public class TestRun {

	public static void main(String[] args) {
		//final Object sharedLock = new Object();
		//ProcessBus processBus = new ProcessBus();
		Lock lock = new ReentrantLock();
		Condition condition = lock.newCondition();

		new Thread(new WorkerProcessB(lock, condition)).start();
		new Thread(new WorkerProcessA(lock, condition)).start();

		// should slowly (300ms a letter) print:
		// AAAAAAAAAABBBBBBBBBB
		// ..as opposite to something like ABAABBABABBABABAA without waiting
		// process B wait ordely for process A to finish
		// Printing A means thread A is active. Printing B menas thred B is active.
	}
}
