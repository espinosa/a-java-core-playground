This is a Java5 concurrency package using version of the SimpleWait example - 2 threads started at the
same time but ThreadB must wait for ThreadA to finish.

Changes:
- ReentrantLock is used instead intrinsic lock. So the lock became Lock class instead plain object.
- The 'bus' from my example became Condition. Usage is more or less same.
- wait() -> await(); notifyAll() -> signalAll()
- Do the waiting and signaling on Condifiton ("bus") 
- Condition must be used inside locked block (in the same way as was synchronized block and lock and wait/invoke, same limitation) 
- unlock in finally block (you don't have to test if the lock is really locked, just unlock)

@author Jan Uhlir (2011 April)
 
