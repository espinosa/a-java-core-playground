package my.code.a001.equality;

/**
 * When it comes to more comparing a not trivial, realistic data structures,
 * then seemingly humble equals() can secretly turn into a complex operation.
 * 
 * Catch is, when one of the business fields (ID like fields or value fields) is
 * itself a complex structure, a custom object, we have to call its equal() -
 * welcome in tricky world of recursion. This opens a whole new set of traps.
 * 
 * In this example we obviously building a cyclic data structure, in real world
 * it may be not so obvious.
 * 
 * Even Joshua Bloch in his Effective Java or Hibernate guys does not mention
 * this yet another equals() catch.
 */
public class B1EqualsWithHiddenInfiniteLoopTest {

	public static void main(String[] args) {
		A a1 = new A();
		B b1 = new B();
		C c1 = new C();
		a1.setFoo(b1);
		b1.setFoo(c1);
		c1.setFoo(a1);

		A a2 = new A();
		B b2 = new B();
		C c2 = new C();
		a2.setFoo(b2);
		b2.setFoo(c2);
		c2.setFoo(a2);

		System.out.println(a1.equals(a2)); // expected FALSE, instead it throws StackOverflowException! 
	}

	public static class A {
		private B foo;

		public void setFoo(B foo) {
			this.foo = foo;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof A))
				return false;
			A other = (A) obj;
			if (foo != null && !foo.equals(other.foo)) // recursion!
				return false;
			return true;
		}
		// hashCode() omitted for brevity
	}

	public static class B {
		private C foo;

		public void setFoo(C foo) {
			this.foo = foo;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof B))
				return false;
			B other = (B) obj;
			if (foo != null && !foo.equals(other.foo)) // recursion!
				return false;
			return true;
		}
		// hashCode() omitted for brevity
	}

	public static class C {
		private A foo;

		public void setFoo(A foo) {
			this.foo = foo;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof C))
				return false;
			C other = (C) obj;
			if (foo != null && !foo.equals(other.foo)) // recursion!
				return false;
			return true;
		}
		// hashCode() omitted for brevity
	}
}
