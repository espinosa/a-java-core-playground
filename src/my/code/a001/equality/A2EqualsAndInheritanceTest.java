package my.code.a001.equality;

/**
 * Equals and inheritance - uneasy relationship. According to Joshua Bloch
 * "there is no way to extend an instantiable class and add a value component
 * while preserving the equals contract" (page 38, Effective Java, item 8)
 * 
 * This example shows would be a solution - use more stricter type check using
 * {@code getClass() != obj.getClass()} instead {@code instanceof} BUT it breaks
 * Liskov substitution principle (LSP).
 * http://en.wikipedia.org/wiki/Liskov_substitution_principle.
 */
public class A2EqualsAndInheritanceTest {

	public static void main(String[] args) {
		A a = new A(1);
		AA aa = new AA(1, 2);
		System.out.println(aa.equals(a)); // false, CORRECT
		System.out.println(a.equals(aa)); // false, CORRECT, symmetry OK

		ProxyOfA ap = new ProxyOfA(1);    // an "innocent" extension of A, just new functionality 
		System.out.println(a.equals(ap)); // false, INCORRECT, they should be equal, breaks LSP 
		System.out.println(ap.equals(a)); // false, INCORRECT, they should be equal, breaks LSP
	}

	public static class A {
		private int f1;

		public A(int f1) {
			this.f1 = f1;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass()) // this makes it fix symmetry 
				return false;
			A other = (A) obj;
			if (f1 != other.f1)
				return false;
			return true;
		}
		// hashCode() was removed for brevity
	}

	public static class AA extends A {
		final int f2;

		public AA(int f1, int f2) {
			super(f1);
			this.f2 = f2;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass()) // this makes it fix symmetry
				return false;
			AA other = (AA) obj;
			if (f2 != other.f2)
				return false;
			return true;
		}
		// hashCode() was removed for brevity
	}

	/** innocent extension, no value components added, just new functionality  */
	public static class ProxyOfA extends A {
		public ProxyOfA(int f1) {
			super(f1);
		}
		public void increase() {
			// omitted for brevity
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			// no own fields to compare, no casting necessary
			return true;
		}
		// hashCode() was removed for brevity
	}
}
